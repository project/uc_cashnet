<?php
// $Id: uc_cashnet.pages.inc,v 1.1.2.5 2010/08/11 17:52:20 islandusurper Exp $

/**
 * @file
 * cashnet menu items.
 *
 */

function uc_cashnet_complete($cart_id = 0) {
  watchdog('uc_cashnet', 'Receiving new order notification for order !order_id.', array('!order_id' => check_plain($_POST['ref1val1'])));

  $order = uc_order_load($_POST['ref1val1']);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    return t('An error has occurred during payment.  Please contact us to ensure your order has submitted.');
  }
  
  // collect total posted by Cashnet
  $total = 0;
  for ($x = 1; $x <= $_POST['itemcnt']; $x++) {
    $total += $_POST['amount'.$x];
  }
  
  // recalculate order total from Ubercart
  $context = array(
    'revision' => 'formatted-original',
    'type' => 'order_total',
    'subject' => array('order' => $order,),
  );
  $options = array(
    'sign' => FALSE, 'dec' => '.', 'thou' => TRUE,
  );
  $order_total = uc_price($order->order_total, $context, $options);

  // check order total against total incoming
  if ($total != $order_total) {
    watchdog('uc_cashnet', 'Incoming total (!total) did not match order #!order_id total of !order_total', array('!total'=>$total, '!order_id'=>$order->order_id, '!order_total'=>$order_total), WATCHDOG_ERROR);
    return t('An error has occurred during payment.  Please contact us to ensure your order has submitted.');
  }
  
  if ($_POST['respmessage'] == 'SUCCESS') {
    $comment = t('Paid by !type, cashnet.com order #!order.', array('!type' => $_POST['pmttype'], '!order' => check_plain($_POST['ref1val1'])));
    uc_payment_enter($order->order_id, 'cashnet', $total, 0, NULL, $comment);
  }
  else {
    drupal_set_message(t('Your order will be processed as soon as your payment clears at cashnet.com.'));
    uc_order_comment_save($order->order_id, 0, t('!type payment is pending approval at cashnet.com.', array('!type' => $_POST['pmttype'])), 'admin');
  }

  // Empty that cart...
  uc_cart_empty($cart_id);

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');

  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  $page = variable_get('uc_cart_checkout_complete_page', '');

  if (!empty($page)) {
    drupal_goto($page);
  }

  return $output;
}
